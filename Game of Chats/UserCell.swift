//
//  UserCell.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 21/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import Firebase
import SnapKit



// ============= Создадим класс для ячейки ==============//
// делаем это для того, чтобы пользоваться нормальным ===//
//методом создания ячейки через dequeReusableCell =======//
/* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */



class UserCell: UITableViewCell {
    
    var message: Message? { // **** start --- сразу определим свойство для установки в него значения из базы
        didSet { setupNameAndAvatar()
            self.detailTextLabel?.text = message?.text
            
            //--- преобразуем timestamp к нормальной дате ---//
            
            if let seconds = message?.timestamp?.doubleValue {
                let timestampDate = Date(timeIntervalSince1970: seconds)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm:ss a"
                timeLabel.text = dateFormatter.string(from: timestampDate)
                
            }
        }
    } // **** end --- сразу определим свойство для установки в него значения из базы
    
    
    
    
    private func setupNameAndAvatar() { // **** start --- установим имя и аватар
        
        if let id = message?.chatPartnerId() {
            
            let ref = Database.database().reference().child("users").child(id)
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let dictionary = snapshot.value as? [String: Any] {
                    self.textLabel?.text = dictionary["name"] as? String
                    
                    if let profileImageUrl = dictionary["profileImageURL"] as? String {
                        self.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
                    }
                }
            }, withCancel: nil)
        }
    } // **** end --- установим имя и аватар
    
    
    
    
    // ******************** start --- устанавливаем view ******************** //
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textLabel?.frame = CGRect(x     : 65,
                                  y     : (textLabel?.frame.origin.y)! - 2,
                                  width : (textLabel?.frame.width)!,
                                  height: (textLabel?.frame.height)!)
        
        detailTextLabel?.frame = CGRect(x     : 65,
                                        y     : (detailTextLabel?.frame.origin.y)! + 2,
                                        width : (detailTextLabel?.frame.width)!,
                                        height: (detailTextLabel?.frame.height)!)
    }
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        //imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = UIColor.lightGray
        //label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        addSubview(profileImageView)
        addSubview(timeLabel)
        
        
        profileImageView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(8)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(50)
        }
        
        timeLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview()
            make.top.equalToSuperview().offset(25)
            make.width.equalTo(100)
            
            if let height = textLabel?.snp.height {
                make.height.equalTo(height)
            }
        }
        
        
        // --- ставим constraints на profileImage в tableView
        // --- как оыбчно потребуется x, y, width, height
//        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
//        profileImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive        = true
//        profileImageView.widthAnchor.constraint(equalToConstant: 50).isActive                  = true
//        profileImageView.heightAnchor.constraint(equalToConstant: 50).isActive                 = true
        
        // --- ставим constraints на timeLabel в tableView
        // --- как оыбчно потребуется x, y, width, height
//        timeLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
//        timeLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 18).isActive    = true
//        timeLabel.widthAnchor.constraint(equalToConstant: 100).isActive                   = true
//        timeLabel.heightAnchor.constraint(equalTo: (textLabel?.heightAnchor)!).isActive   = true

    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // ******************* end --- устанавливаем view ******************** //
    
    
}






/* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
