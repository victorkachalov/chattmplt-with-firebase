//
//  NewMessageController.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 19/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import Firebase

class NewMessageController: UITableViewController {

    
    
    let cellIdentifier = "cell"
    
    var users = [User]()
    var messagesController: MessagesController?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // --- ставим кнопку Cancel
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel",
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(handleCancel))
        
        // --- устанавливаем для таблицы класс ячейки
        tableView.register(UserCell.self, forCellReuseIdentifier: cellIdentifier)
        
        fetchUser()
    }
    
    
    
    
    func fetchUser() { // начало: --- сетевая функция для подтягивания юзеров из базы данных
        
        Database.database().reference().child("users").observe(.childAdded, with: { snapshot in
            
            if let dictionary = snapshot.value as? [String: AnyObject] { //--- примечание: ключи из Firebase должны совпасть с теми ключами, которые будут указаны в классе, иначе краш
                let user = User()
                user.id = snapshot.key
                user.setValuesForKeys(dictionary)
                self.users.append(user)
                
                // --- выполнение идет на бэкграунд треде, UI всегда собирается на main
                // --- чтобы не было краша, переведем на главный тред асинхронно
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
        }, withCancel: nil)
        
    } // конец: --- сетевая функция для подтягивания юзеров из базы данных
    
    
    
    
    
    func handleCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
    // ----------- Начало: создаем таблицу с юзерами ----------
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //let cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier) 
        // --- так как выше инициализировать клетку не следует, нужно делать через deque, для этого создадим класс UserCell
        // --- делаем этот класс для того, чтобы мы могли выбрать стиль ячейки subtitile, куда будем вписывать email юзеров
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? UserCell
        
        let user                     = users[indexPath.row]
        cell?.textLabel?.text        = user.name
        cell?.detailTextLabel?.text  = user.email
        cell?.imageView?.contentMode = .scaleAspectFill
        
        if let profileImageUrl = user.profileImageURL {
            cell?.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        }
        
        return cell!
        
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
    // ----------- Конец: создаем таблицу с юзерами -----------
    
    
    
    
    

    // --- Начало: нажимая на юзера в таблице откроем чат с ним ---
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            let user = self.users[indexPath.row]
            self.messagesController?.showChatControllerForUser(user: user)
        }
    }

    // --- Конец: нажимая на юзера в таблице откроем чат с ним ---

    
}
