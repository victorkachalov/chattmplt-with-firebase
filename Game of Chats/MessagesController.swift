//
//  ViewController.swift
//  Game of Chats
//
//  Created by Victor Kachalov on 15.09.17.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import Firebase

class MessagesController: UITableViewController {

    
    var messages = [Message]()
    var messageDictionary = [String: Message]()
    
    let cellId = "cell"
    
    var timer: Timer?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //--- устанавливаем в NavigationBar кнопку Logout
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout",
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(handleLogout))
        
        //--- устанавливаем в NavigationBar кнопку New message
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "msg.png"),
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(handleNewMessage))
        
        
        checkIfUserIsLoggedIn()
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        tableView.allowsSelectionDuringEditing = true
        
    }
    
    
    
    
    
    func checkIfUserIsLoggedIn () { // start: --- проверка юзера на авторизацию
        
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
            //handleLogout()
        } else {
            fetchUserAndSetupNavBarTitle()
        }
        
    } //  end: --- проверка юзера на авторизацию
    
    
    
    
    
    func fetchUserAndSetupNavBarTitle() { // start: --- установка имени юзера на НавБар
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { snapshot in
            
            if let dictionary = snapshot.value as? [String: Any] {
                
                let user = User()
                user.setValuesForKeys(dictionary)
                self.setupNavBarWithUser(user: user)
                
            }
            
        }, withCancel: nil)

    } // end: --- установка имени юзера на НавБар
    
    
    
    
    
    
    //-----------------  Начало: настройка UI ---------------//
    /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
    
    
    func setupNavBarWithUser(user: User) { // --- start: будем устанавливать аватар и имя юзера в тайтл нав бар
        
        //чистим наш список чатов
        
        messages.removeAll()
        messageDictionary.removeAll()
        tableView.reloadData()
        
        // снова заполним списком чатов
        
        observeUserMessages()

        
        let titleView = UIView()
        
        titleView.frame = CGRect(x     : 0,
                                 y     : 0,
                                 width : 100,
                                 height: 40)
        
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        titleView.addSubview(containerView)
        
        let profileImageView = UIImageView()
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.contentMode = .scaleAspectFill
        
        /*  делаем закгругления рамки для картинки, 
            всегда такая парная запись ---------- */
        profileImageView.layer.cornerRadius = 20
        profileImageView.layer.masksToBounds = true
        
        if let profileImageUrl = user.profileImageURL {
            profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        }
        
        containerView.addSubview(profileImageView)
        
        // --- установим constraints на profileImageView в нав баре
        // --- потребуется x, y, width, height
        profileImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive       = true
        profileImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 40).isActive                    = true
        profileImageView.heightAnchor.constraint(equalToConstant: 40).isActive                   = true
        
        let nameLabel = UILabel()
        
        containerView.addSubview(nameLabel)
        
        nameLabel.text = user.name
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // --- установим constraints на nameLabel в нав баре
        // --- потребуется x, y, width, height
        nameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 10).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive          = true
        nameLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive                 = true
        nameLabel.heightAnchor.constraint(equalTo: profileImageView.heightAnchor).isActive            = true
        
        containerView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        containerView.centerXAnchor.constraint(equalTo: titleView.centerXAnchor).isActive = true
        
        self.navigationItem.titleView = titleView
        
        //titleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showChatControllerForUser)))
        
    } // --- end: будем устанавливать аватар и имя юзера в тайтл нав бар
    
    
    
    /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
    //-----------------  Конец : настройка UI ----------------//
    
    
    
    
    
    
    func showChatControllerForUser(user: User) { // --- start: переход на chatLogController
        
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        
        chatLogController.user = user
        
        // ------- аналог сегвея через навБар ----------------
        navigationController?.pushViewController(chatLogController, animated: true)
        
    } // --- end: переход на chatLogController
    
    
    
    
    
    func handleLogout() { // start: --- выполнение при нажатии Logout
        
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
        let loginController = LoginController()
        loginController.messagesController = self
        
        /* переходим на экран логин/регистрация */
        // ------- аналог сегвея ----------------
        present(loginController, animated: true, completion: nil)
    
    }// end: --- выполнение при нажатии Logout
    
    
    
    
    
    func observeUserMessages() { // --- начало: наполняем чатами нашего залогиненного юзера
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let ref = Database.database().reference().child("user-messages").child(uid)
        
        ref.observe(.childAdded, with: { (snapshot) in
            
            let userId = snapshot.key // --- по ключу мы узнаем айдишник
            Database.database().reference().child("user-messages").child(uid).child(userId).observe(.childAdded, with: { (snapshot) in
                
                let messageId = snapshot.key
                self.fetchMessageWithMessageId(messageId: messageId)
                
            }, withCancel: nil)
            
        }, withCancel: nil)
        
        ref.observe(.childRemoved, with: { (snapshot) in
            
            self.messageDictionary.removeValue(forKey: snapshot.key)
            self.attemptReloafOfTable()
            
        }, withCancel: nil)
        
    } // --- конец: наполняем чатами нашего залогиненного юзера
    
    
    
    
    
    private func fetchMessageWithMessageId (messageId: String) { // старт: --- получаем сообщением по айди
        
        let messageReference = Database.database().reference().child("messages").child(messageId)
        
        messageReference.observeSingleEvent(of: .value, with: { (snapshot) in
            
            print(snapshot)
            if let dictionary = snapshot.value as? [String: Any] {
                
                let message = Message(dictionary: dictionary)
                
                if let chatPartnerId = message.chatPartnerId() { // --- наполняем значениями +
                    
                    self.messageDictionary[chatPartnerId] = message
                    
                } // --- наполняем значениями -
                
                //проверка что timestamp все таки UInt64
                if let timestamp = message.timestamp as? UInt64 {
                    print(timestamp)
                }//--- работает
                
                self.attemptReloafOfTable()
            }
            
        }, withCancel: nil)
        
    } // конец: --- получаем сообщением по айди
    
    
    
    
    
    private func attemptReloafOfTable() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    
    
    
    
    func handleReloadTable() { // начало: ---  обновляем таблицу
        
        self.messages = Array(self.messageDictionary.values)
        self.messages.sort(by: { (message1, message2) -> Bool in
            
            return (message1.timestamp?.intValue)! > (message2.timestamp?.intValue)!
            
        })
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    } // конец: ---  обновляем таблицу
    
    
    
    

    func handleNewMessage() { // --- start: go to the NewMessageVC
        
        let newMessageController = NewMessageController()
        newMessageController.messagesController = self
        let navController = UINavigationController(rootViewController: newMessageController)
        
        present(navController, animated: true, completion: nil)
        
    } // --- end: go to the NewMessageVC
    
    
    
    
    //--------------  Начало: настройка таблицы -------------//
    /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        let message = messages[indexPath.row]
        cell.message = message
        return cell
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let message = messages[indexPath.row]
        
        guard let chatPartnerId = message.chatPartnerId() else {
            return
        }
        
        let ref = Database.database().reference().child("users").child(chatPartnerId)
        
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: Any] else {
                return
            }
            
            let user = User()
            user.id = chatPartnerId
            user.setValuesForKeys(dictionary)
            
            self.showChatControllerForUser(user: user)
            
        }, withCancel: nil)
        
    }
    
    
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let message = self.messages[indexPath.row]
        
        if let chatPartnerId = message.chatPartnerId() {
            Database.database().reference().child("user-messages").child(uid).child(chatPartnerId).removeValue(completionBlock: { (error, ref) in
                
                if error != nil {
                    print(error)
                }
                
                self.messageDictionary.removeValue(forKey: chatPartnerId)
                self.attemptReloafOfTable()
                
            })
        }
        
        
    }

    
    /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
    //----------------  Конец: настройка таблицы -------------//
    
}

