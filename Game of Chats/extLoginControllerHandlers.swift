//
//  LoginControllerHandlers.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 19/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import Firebase

extension LoginController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
    // ---- START: работа с прикреплением фото из галереи ---//
    
    
    
    func handleSelectProfileImageView() { // --- start: запуск выбора фотки из галереии
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker, animated: true, completion: nil)
        
    } // --- end: запуск выбора фотки из галереии
    
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) { // --- start: отмена выбора фотки из галереии
        
        print("dismissed")
        dismiss(animated: true, completion: nil)
        
    } // --- end: отмена выбора фотки из галереии
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) { // --- start: ставим фотку в профиль после выбора фотки из галереии
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            print("edited image")
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print("orirginal image")
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            print("image chosen")
            profileImageView.image = selectedImage
        }
        
        // --- обязательно делаем дисмисс после выбора, 
        // --- иначе экран с галереей не закроется
        dismiss(animated: true, completion: nil)
        
    } // --- end: ставим фотку в профиль после выбора фотки из галереии
    
    
    
    /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
    // ---- START: работа с прикреплением фото из галереи ---//
    
    
    
    
    
    
    
    /* ======================== MARK: Start: Auth / Working with Firebase =============================== */
                        /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
    
    
    
    func handleRegister() { //--- start: регистрируем нового пользователя в Firebase
        
        guard let email = emailTextField.text,
            let password = passwordTextField.text,
            let name = nameTextField.text else {
                print("Form is not valid")
                return
        }
        
        Auth.auth().createUser(withEmail: email,
                               password: password,
                               completion: { (user, error) in
                                
                                if error != nil {
                                    print(error)
                                    return
                                }
                                
                                guard let uid = user?.uid else { return }
                                
                                // --- запишем для юзера его профайл имедж в сторедж
                                let imageName = UUID.init().uuidString
                                let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).jpg")
                                
                                if let profileImage = self.profileImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.5) {
                                    
                                    storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                                        
                                        if error != nil  {
                                            print(error)
                                            return
                                        }
                                        
                                        if let profileImageUrl = metadata?.downloadURL()?.absoluteString {
                                            let values = ["name" : name, "email": email, "profileImageURL": profileImageUrl]
                                            self.registerUserIntoDatabaseWithUID(uid: uid, values: values as [String : AnyObject])
                                        }
                                    })
                                }
        })
        
    }
    
    
    
    private func registerUserIntoDatabaseWithUID (uid: String, values: [String: AnyObject]) {
        
        // --- запишем юзера в базу
        let ref = Database.database().reference()
        let usersReference = ref.child("users").child(uid)
        usersReference.updateChildValues(values) { (error, ref) in
            
            if error != nil  {
                print(error)
                return
            }
            
            let user = User()
            user.setValuesForKeys(values)
            self.messagesController?.setupNavBarWithUser(user: user)
            
            // --- после регистрации сворачиваем окно регистрации/логина
            self.dismiss(animated: true, completion: nil)
            
        }
        
    } //--- end: регистрируем нового пользователя в Firebase
    
    
    
    
    
    func handleLoginRegister() { // start: в зависимости от статуса командной кнопки: логинимся или регимся
        
        if loginRegisterSegmentControl.selectedSegmentIndex == 0 {
            handleLogin()
        } else {
            handleRegister()
        }
        
    } // end: в зависимости от статуса командной кнопки: логинимся или регимся
    
    
    
    
    
    func handleLogin() { // --- start: совершаем логин
        
        guard let email = emailTextField.text,
            let password = passwordTextField.text else {
                print("Form is not valid")
                return
        }
        
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            
            if error != nil {
                print(error)
                return
            }
            
            //если удачно залогинились
            self.messagesController?.fetchUserAndSetupNavBarTitle()
            self.dismiss(animated: true, completion: nil)
            
        })
        
    } // --- end: совершаем логин
    
    
    
    
                        /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
    /* ============================= End : Auth / Working with Firebase ================================= */
    
    
    
    
    func handleLoginRegisterChange() { // start: --- изменяем  UI в зависимости от выбранной кнопки (регистрация/логин)
        
        let title = loginRegisterSegmentControl.selectedSegmentIndex
        loginRegisterButton.setTitle(loginRegisterSegmentControl.titleForSegment(at: title), for: .normal)
        
        // изменить высоту блока с именем/емейл/пароль
        inputsContainerViewHeigtAnchor?.constant = loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 100 : 150
        
        // изменить высоту поля ввода имени
        nameTextFieldHeightAnchor?.isActive = false
        nameTextFieldHeightAnchor = nameTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 0 : 1/3)
        nameTextFieldHeightAnchor?.isActive = true
        
        if loginRegisterSegmentControl.selectedSegmentIndex == 0 {
            nameTextField.placeholder = nil
            nameTextField.text = nil
        } else {
            nameTextField.placeholder = "Name"
        }
        
        nameSeparatorViewHeightAnchor?.isActive = false
        nameSeparatorViewHeightAnchor = nameSeparatorView.heightAnchor.constraint(equalToConstant: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 0 : 1)
        nameSeparatorViewHeightAnchor?.isActive = true
        
        // изменить высоту поля ввода email
        emailTextFieldHeightAnchor?.isActive = false
        emailTextFieldHeightAnchor = emailTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        emailTextFieldHeightAnchor?.isActive = true
        
        // изменить высоту поля ввода password
        passwordTextFieldHeightAnchor?.isActive = false
        passwordTextFieldHeightAnchor = passwordTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        passwordTextFieldHeightAnchor?.isActive = true
        
    } // end: --- изменяем  UI в зависимости от выбранной кнопки (регистрация/логин)
    
}
