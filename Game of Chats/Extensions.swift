//
//  Extensions.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 20/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import SDWebImage

/* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
// ------ РАБОТА С СОХРАНЕНИМ В КЭШ КАРТИНОК-АВАТАРОК ---//



extension UIImageView {
    
    func loadImageUsingCacheWithUrlString(urlString: String) { // --- start: записываем картинки в кэш
     
        self.sd_setImage(with: URL(string: urlString), placeholderImage: self.image, options: .continueInBackground, completed: nil)

    } // --- end: записываем картинки в кэш
    
}


/* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
// ------ РАБОТА С СОХРАНЕНИМ В КЭШ КАРТИНОК-АВАТАРОК ---//










//let imageCache = NSCache<NSString, UIImage>()
//let defaults   = UserDefaults.standard

//**** БЫЛО:
    
// проверим есть ли картинка в кэше
//        
//                    if let cachedImage = imageCache.object(forKey: urlString as NSString) {
//                        self.image = cachedImage
//                        return
//                }
        
        
        
        
        /* если нет в кэше то закачаем в кэш */
        
//        let url = URL(string: urlString)
//        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
//
//            if error != nil { // если ошибка, то возврат
//                print(error)
//                return
//            }
//            
//            DispatchQueue.main.async {
//                if let downloadedImage = UIImage(data: data!) {
//                    imageCache.setObject(downloadedImage, forKey: urlString as NSString)
//                    self.image = downloadedImage
//                }
//            }
//            
//            defaults.set(data, forKey: "SavedImage")
//            
//        }).resume()



