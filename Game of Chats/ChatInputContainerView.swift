//
//  ChatInputContainerView.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 29/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import SnapKit

class ChatInputContainerView: UIView, UITextFieldDelegate {

    
    let uploadImageView : UIImageView = {
        let uploadImageView = UIImageView()
        uploadImageView.isUserInteractionEnabled = true
        uploadImageView.image = UIImage(named: "image.png")
        //uploadImageView.translatesAutoresizingMaskIntoConstraints = false
        return uploadImageView
    }()
    
    let sendButton: UIButton = {
        let sendButton = UIButton(type: .system)
        sendButton.setTitle("Send", for: .normal)
        //sendButton.translatesAutoresizingMaskIntoConstraints = false
        return sendButton
    }()
    
    let separatorView: UIView = {
        let separatorView = UIView()
        separatorView.backgroundColor = UIColor(red: 220/225,
                                                green: 220/225,
                                                blue: 220/225,
                                                alpha: 1)
        //separatorView.translatesAutoresizingMaskIntoConstraints = false
        return separatorView
    }()
    
    var chatLogController: ChatLogController? {
        didSet {
            sendButton.addTarget(chatLogController, action: #selector(ChatLogController.handleSend), for: .touchUpInside)
            uploadImageView.addGestureRecognizer(UITapGestureRecognizer(target: chatLogController, action: #selector(ChatLogController.handleUploadTap)))
        }
    }
    
    lazy var inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter message..."
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        /* Для использования должны добавить UITextFieldDelegate */
        /* делаем для того, чтобы можно было в дальнейшем        */
        /* отправлять сообщения при нажатии Return               */
        /* описано в функции textFieldShouldReturn               */
        /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
        
        textField.delegate = self
        
        /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
        
        return textField
    }()

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        addSubview(uploadImageView)
        addSubview(sendButton)
        addSubview(inputTextField)
        addSubview(separatorView)
        
        
        uploadImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(30)
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(8)
        }
        
        inputTextField.snp.makeConstraints { (make) in
            make.left.equalTo(uploadImageView.snp.right).offset(8)
            make.right.equalTo(sendButton.snp.left)
            make.centerY.height.equalToSuperview()
        }
        
        sendButton.snp.makeConstraints { (make) in
            make.width.equalTo(80)
            make.right.centerY.height.equalToSuperview()
        }
        
        separatorView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(1)
            make.centerY.equalTo(self.snp.top)
        }
        
        // --- setup constraints for sendButton
        // --- x, y, height, width are needed as usual
//        uploadImageView.translatesAutoresizingMaskIntoConstraints = false
//        uploadImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive       = true
//        uploadImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        uploadImageView.widthAnchor.constraint(equalToConstant: 30).isActive                    = true
//        uploadImageView.heightAnchor.constraint(equalToConstant: 30).isActive                   = true
        

        // --- setup constraints for sendButton
        // --- x, y, height, width are needed as usual
//        sendButton.rightAnchor.constraint(equalTo: rightAnchor).isActive     = true
//        sendButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        sendButton.widthAnchor.constraint(equalToConstant: 80).isActive                    = true
//        sendButton.heightAnchor.constraint(equalTo: heightAnchor).isActive   = true
        
        
        // --- setup constraints for inputTextField
        // --- x, y, height, width are needed as usual
//        inputTextField.leftAnchor.constraint(equalTo: uploadImageView.rightAnchor, constant: 8).isActive = true
//        inputTextField.centerYAnchor.constraint(equalTo: centerYAnchor).isActive        = true
//        inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive                = true
//        inputTextField.heightAnchor.constraint(equalTo: heightAnchor).isActive          = true
        

        // --- setup constraints for separatorView
        // --- x, y, height, width are needed as usual
//        separatorView.leftAnchor.constraint(equalTo: leftAnchor).isActive   = true
//        separatorView.centerYAnchor.constraint(equalTo: topAnchor).isActive = true
//        separatorView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
//        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive  = true
        
    }
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { // --- start: отправка сообщений по Return
        
        chatLogController?.handleSend()
        return true
        
    } // --- end: отправка сообщений по Return
    
    
}
