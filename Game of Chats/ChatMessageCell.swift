//
//  ChatMessageCell.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 22/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import AVFoundation
import SnapKit

// ============= Создадим класс для ячейки ==============//
// в collectionView, будем преобразовывать к красивому ==//
// ============= виду сообщений в виде облаков ==========//
/* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */




class ChatMessageCell: UICollectionViewCell {
    
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var message: Message?
    var chatLogController: ChatLogController?
    
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        //aiv.translatesAutoresizingMaskIntoConstraints = false
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    lazy var playButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Play Video", for: .normal)
        //button.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "playbtn")
        button.setImage(image, for: .normal)
        button.tintColor = UIColor.white
        
        button.addTarget(self, action: #selector(handlePlay), for: .touchUpInside)
        
        return button
    }()
    
    
    
    
    func handlePlay() {
        if let videoUrlString = message?.videoUrl, let url = URL(string: videoUrlString) {
            
            player = AVPlayer(url: url)
            
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = bubbleView.bounds
            
            bubbleView.layer.addSublayer(playerLayer!)
            
            player?.play()
            activityIndicatorView.startAnimating()
            playButton.isHidden = true
            
        }
    }
    
    
    
    
    // --- делаем для того, чтобы при прокрутке, медиа перестала воспроизводится

    override func prepareForReuse() {
        super.prepareForReuse()
        playerLayer?.removeFromSuperlayer()
        player?.pause()
        activityIndicatorView.stopAnimating()
    }
    
    
    
    
    let textView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 16)
        //tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = UIColor.clear
        tv.textColor = UIColor.white
        tv.isEditable = false
        return tv
    }()
    
    static let blueColor = UIColor(red: 0/255,
                                   green: 137/255,
                                   blue: 249/255,
                                   alpha: 1)
    
    let bubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = blueColor
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
        return view
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        //imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var messageImageView: UIImageView = {
        let imageView = UIImageView()
        //imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomTap)))
        
        return imageView
    }()
    
    func handleZoomTap(tapGesture: UITapGestureRecognizer) {
        if message?.videoUrl != nil {
            return
        }
        if let imageView = tapGesture.view as? UIImageView {
            self.chatLogController?.performZoomInForStartingImageView(startingImageView: imageView)
        }
    }
    
    var bubbleWidthAnchor    : NSLayoutConstraint?
    var bubbleViewRightAnchor: NSLayoutConstraint?
    var bubbleViewLeftAnchor : NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(bubbleView)
        addSubview(textView)
        addSubview(profileImageView)
        bubbleView.addSubview(messageImageView)
        bubbleView.addSubview(playButton)
        bubbleView.addSubview(activityIndicatorView)
        
        messageImageView.snp.makeConstraints { (make) in
            make.left.top.width.height.equalToSuperview()
        }
        
        
        // --- ставим constraints на bubbleView в collectionView
        // --- как оыбчно потребуется x, y, width, height
        
        bubbleViewRightAnchor = bubbleView.rightAnchor.constraint(equalTo: self.rightAnchor,
                                                                  constant: -8)
        bubbleViewLeftAnchor = bubbleView.leftAnchor.constraint(equalTo: profileImageView.rightAnchor,
                                                                constant: 8)
        bubbleView.topAnchor.constraint(equalTo: self.topAnchor).isActive       = true
        bubbleWidthAnchor = bubbleView.widthAnchor.constraint(equalToConstant: 200)
        bubbleWidthAnchor?.isActive                                             = true
        bubbleView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        playButton.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(50)
        }
        
        activityIndicatorView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(50)
        }
        
        textView.snp.makeConstraints { (make) in
            make.top.height.equalToSuperview()
            make.left.equalTo(bubbleView.snp.left).offset(8)
            make.right.equalTo(bubbleView.snp.right)
        }
        
        profileImageView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(8)
            make.width.height.equalTo(32)
        }
        
        
        // --- ставим constraints на messageImageView в bubbleView
        // --- как оыбчно потребуется x, y, width, height
//        messageImageView.leftAnchor.constraint(equalTo: bubbleView.leftAnchor).isActive     = true
//        messageImageView.topAnchor.constraint(equalTo: bubbleView.topAnchor).isActive       = true
//        messageImageView.widthAnchor.constraint(equalTo: bubbleView.widthAnchor).isActive   = true
//        messageImageView.heightAnchor.constraint(equalTo: bubbleView.heightAnchor).isActive = true

        // --- ставим constraints на playButton в bubbleView
        // --- как оыбчно потребуется x, y, width, height
//        playButton.centerXAnchor.constraint(equalTo: bubbleView.centerXAnchor).isActive = true
//        playButton.centerYAnchor.constraint(equalTo: bubbleView.centerYAnchor).isActive = true
//        playButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        playButton.heightAnchor.constraint(equalToConstant: 50).isActive = true

        // --- ставим constraints на playButton в bubbleView
        // --- как оыбчно потребуется x, y, width, height
//        activityIndicatorView.centerXAnchor.constraint(equalTo: bubbleView.centerXAnchor).isActive = true
//        activityIndicatorView.centerYAnchor.constraint(equalTo: bubbleView.centerYAnchor).isActive = true
//        activityIndicatorView.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        activityIndicatorView.heightAnchor.constraint(equalToConstant: 50).isActive = true

        // --- ставим constraints на textView в collectionView
        // --- как оыбчно потребуется x, y, width, height
//        textView.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
//        textView.topAnchor.constraint(equalTo: self.topAnchor).isActive                      = true
//        textView.rightAnchor.constraint(equalTo: bubbleView.rightAnchor).isActive            = true
//        textView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive                = true

        // --- ставим constraints на profileImageView в collectionView
        // --- как оыбчно потребуется x, y, width, height
//        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
//        profileImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive          = true
//        profileImageView.widthAnchor.constraint(equalToConstant: 32).isActive                  = true
//        profileImageView.heightAnchor.constraint(equalToConstant: 32).isActive                 = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


/* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
